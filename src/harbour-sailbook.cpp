#include <QtQuick>
#include <sailfishapp.h>
#include <libsailfishwebengine/webengine.h>
#include <libsailfishwebengine/webenginesettings.h>
#include "useragent.h"

using namespace SailfishOS;

static void showHelp()
{
    qDebug() << "Usage: cmd --help | [URL]...";
}

int main(int argc, char *argv[])
{
    if ((argc > 2) || ((argc == 2) && (strcmp("--help", argv[1]) == 0))) {
        showHelp();
        return 0;
    }

    QString startUrl;
    if (argc == 2) {
        startUrl = QString(argv[1]);
    }
    else {
        startUrl = QStringLiteral("https://duckduckgo.com/?q=useragent&ia=answer");
    }
    qDebug() << "Start URL: " << startUrl;

    QGuiApplication *app = SailfishApp::application(argc, argv);

    QQuickView *view = SailfishApp::createView();
    view->setSource(SailfishApp::pathTo("qml/harbour-sailbook.qml"));
    QQmlContext *ctxt = view->rootContext();
    ctxt->setContextProperty("startUrl", startUrl);
    view->show();

    WebEngineSettings *wes = SailfishOS::WebEngineSettings::instance();
    wes->setPreference(QString("general.useragent.override.duckduckgo.com"), "A new test");

    return app->exec();
}
