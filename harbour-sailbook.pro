TARGET = harbour-sailbook

CONFIG += link_pkgconfig
PKGCONFIG += qt5embedwidget sailfishwebengine

SOURCES += src/harbour-sailbook.cpp

DISTFILES += qml/harbour-sailbook.qml \
    qml/cover/CoverPage.qml \
    qml/pages/WebViewPage.qml \
    rpm/harbour-sailbook.changes.in \
    rpm/harbour-sailbook.changes.run.in \
    rpm/harbour-sailbook.spec \
    rpm/harbour-sailbook.yaml \
    translations/*.ts \
    harbour-sailbook.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/harbour-sailbook-de.ts

DISTFILES += \
    qml/pages/AboutPage.qml \
    qml/components/IconTextButton.qml \
    qml/components/TextLabel.qml \
    qml/js/media.js \
    qml/js/messages.js \
    qml/js/util.js

RESOURCES += \
    qml/resources/resources.qrc
