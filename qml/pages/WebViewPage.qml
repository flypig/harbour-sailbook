import QtQuick 2.2
import Sailfish.Silica 1.0
import Sailfish.WebView 1.0
import "../components"

Page {
    id: page

    allowedOrientations: Orientation.All

    backNavigation: false
    forwardNavigation: false
    showNavigationIndicator: false

    SilicaFlickable {
        anchors.fill: parent

    PullDownMenu {
        MenuItem {
            text: qsTr("About")
            onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
        }
    }

    WebView {
        id: webView
        anchors.fill: parent

        active: true
        url: "http://www.facebook.com"

        onViewInitialized: {
            webView.loadFrameScript(Qt.resolvedUrl("qrc:///js/sailbook.js"));
        }
      }
    }
  }
